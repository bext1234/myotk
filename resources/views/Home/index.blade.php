<link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css">
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.7.0.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>

   <meta charset="UTF-8">
   <title>Your Laravel App</title>

</head>
<body>
    <div class="text-center text-2xl">
        <h1><b>คำถาม ROO</b></h1>
    </div>

    <div class="fw-container">
        <div class="fw-body">
            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>question</th>
                        <th>answer</th>
                    </tr>
                </thead>
                <tbody id="listData">
                </tbody>
            </table>
        </div>
    </div>

    
    <script type="text/javascript">

        $.ajax({
            type: 'POST',
            url: "{{ route('index-search') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
            processData: false,
            dataType: 'html',
            contentType: 'application/json',
            success: (data) => {
                $('#listData').html(data);
                $('#example').DataTable();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(ajaxOptions);
                console.log(thrownError);
            }
        });
    </script>
</body>
</html>