<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Models\question_roo;

class HomeController extends Controller
{
    public function index()
    {
        return view('home/index');
    }

    public function search(Request $request){
        $lstData = question_roo::get();
        return view('home/_listData',compact('lstData'));
    }
}
